package com.example.realhousehelps;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import androidx.appcompat.app.AppCompatActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Payment extends AppCompatActivity {
    Button make_payment;
    EditText textView;
    String amt;
    String userid="0725850798";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        textView=findViewById(R.id.amount);
        amt=textView.getText().toString();

        make_payment=findViewById(R.id.pay);
        make_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("......"+amt);
                showDialog(Payment.this,"Confirmed you want to pay "+ amt +" to househelp account");

            }
        });


    }


// mke payment for the housegirl
    private void makepayment() {






    }

    public void showDialog(Activity activity, String msg){
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);

        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
        text.setText(msg);

        Button cancel = (Button) dialog.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        Button dialogButton = (Button) dialog.findViewById(R.id.btn_pay);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //call function to send request to push stk.
                Toast.makeText(getApplicationContext(),"Your Requst is Successful wait for pin Authorization",Toast.LENGTH_LONG).show();
               dialog.dismiss();

            }
        });

        dialog.show();

    }
}
