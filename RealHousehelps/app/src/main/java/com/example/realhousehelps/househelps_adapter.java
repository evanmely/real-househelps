package com.example.realhousehelps;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.example.realhousehelps.APIs.APIs;

import java.util.ArrayList;
import java.util.List;

public class househelps_adapter extends  RecyclerView.Adapter<househelps_adapter.househelpsViewHolder> implements Filterable{
    //
    private Context ctx;
    private List<househelps_model>HouseList;
    private List<househelps_model> HouseListFiltered;
    RequestOptions option;
    private OnItemClickListener mlistener;

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    HouseListFiltered = HouseList;
                } else {
                    List<househelps_model> filteredList = new ArrayList<>();
                    for (househelps_model row : HouseList) {
                        // here we are looking for placement type , location ,, match
                        if (row.getType().toLowerCase().contains(charString.toLowerCase()) || row.getFirst_name().toLowerCase().contains(charString.toLowerCase())||
                                row.getGender().toLowerCase().contains(charString.toLowerCase()) || row.getMarital_status().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }
                    HouseListFiltered = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = HouseListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                HouseListFiltered = (ArrayList<househelps_model>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    public interface OnItemClickListener{
        void OnItemClick(int position);
    }
public  void setOnItemClickListener(OnItemClickListener listener){
        mlistener= listener;

}
    public househelps_adapter(Context ctx, List<househelps_model> HouseList) {
        this.ctx = ctx;
        this.HouseList = HouseList;
        this.HouseListFiltered = HouseList;
        option = new RequestOptions().centerCrop().placeholder(R.drawable.man).error(R.drawable.man);
    }
    @NonNull
    @Override
    public househelpsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(ctx);
        View view = inflater.inflate(R.layout.view_househelps, null);
        return new househelpsViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull househelpsViewHolder holder, int i) {
        final househelps_model househelpfilter = HouseListFiltered.get(i);
        String thumbnail = HouseList.get(i).getImgURL();
        Glide.with(ctx)
                .load(APIs.Passport_Url +thumbnail)
                .thumbnail(0.5f)
                .fitCenter()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.imageView);

        holder.name.setText(househelpfilter.getFirst_name());
        holder.textdob.setText(househelpfilter.getDob());
        holder.texttype.setText(String.valueOf(househelpfilter.getType()));
        holder.textStatus.setText(String.valueOf(househelpfilter.getMarital_status()));
holder.txtgender.setText(String.valueOf(househelpfilter.getGender()));
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ctx,househelp_details.class);
                intent.putExtra("id",househelpfilter.getId());
                ctx.startActivity(intent);
            }
        });

    }
    @Override
    public int getItemCount() {
        return HouseListFiltered.size();
    }

    class househelpsViewHolder extends  RecyclerView.ViewHolder{
        TextView name, textdob, texttype, txtgender,textStatus;
       ImageView imageView;

     public househelpsViewHolder(@NonNull View itemView) {
         super(itemView);
         name = itemView.findViewById(R.id.name);
         textdob = itemView.findViewById(R.id.dob);
         txtgender = itemView.findViewById(R.id.gender);
         textStatus=itemView.findViewById(R.id.status);
         texttype= itemView.findViewById(R.id.type);
         imageView = itemView.findViewById(R.id.imageView);
         imageView.buildDrawingCache();
         Bitmap bmp = imageView.getDrawingCache();
         itemView.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 if(mlistener!=null){
                     int position= getAdapterPosition();
                     if(position!=RecyclerView.NO_POSITION){
                         mlistener.OnItemClick(position);
                     }
                 }
             }
         });
     }
 }


    }