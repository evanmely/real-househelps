package com.example.realhousehelps;
import androidx.appcompat.app.AppCompatActivity;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.realhousehelps.APIs.APIs;
import com.example.realhousehelps.controllers.AppController;
import com.example.realhousehelps.helper.SQLiteHandler;
import com.example.realhousehelps.helper.SessionManager;
import com.example.realhousehelps.helper.session_manager;
import com.example.realhousehelps.model.JsonResponse;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;
public class Register extends AppCompatActivity {
    private ProgressDialog pDialog;
    private session_manager session;
    //private SQLiteHandler db;
Button  btnregister;
EditText  txtname,txtemail,txtphone,txtpassword;
//TextInputEditText

//EditText txtnames,txtemail,txtphone,txtpassword;
private static final String TAG = Register.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        btnregister=findViewById(R.id.btnsign_up);
        txtname=findViewById(R.id.names);
        txtemail=findViewById(R.id.email_address);
        txtphone=findViewById(R.id.phone);
        txtpassword=findViewById(R.id.password);
        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        // Session manager
        session = new session_manager(getApplicationContext());

        // SQLite database handler
        //db = new SQLiteHandler(getApplicationContext());

       //  Check if user is already logged in or not
        session.checkLogin();
//        finish();
        btnregister.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String name = txtname.getText().toString().trim();
                String email = txtemail.getText().toString().trim();
                String phone = txtphone.getText().toString().trim();
                String password = txtpassword.getText().toString().trim();

                if (!name.isEmpty() && !email.isEmpty() &&!phone.isEmpty() && !password.isEmpty()){
                    registerUser(name, email,phone, password);
                }
                else {
                    Toast.makeText(getApplicationContext(),
                            "Please enter your details!", Toast.LENGTH_LONG)
                            .show();
                }
            }
        });

    }
    private void registerUser(final String name, final String email,final String phone,
                              final String password) {
        Log.e(TAG, "registerUser: start");
        // Tag used to cancel the request
        String tag_string_req = "req_register";

        pDialog.setMessage("Registering ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                APIs.Register_Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Register Response: " + response);
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("success");

                    Log.e(TAG, "onResponse: " + error);
                    if (error) {
                        // User successfully stored in MySQL
                        // Now store the user in sqlite
                        Log.e(TAG, "onResponse: success");
                        JSONObject access_token = jObj.getJSONObject("token");
                        String uid = access_token.getString("access_token");
                        JSONObject user = jObj.getJSONObject("user");
                        String name = user.getString("name");
                        String email = user.getString("email");
                        String phone = user.getString("phone");
                        String created_at = user
                                .getString("created_at");

                        Toast.makeText(getApplicationContext(), "User successfully registered!", Toast.LENGTH_LONG).show();
                        // Launch login activity
                        startActivity(new Intent(getApplicationContext(),MainActivity.class));
                        finish();
                    }
                    else
                        {
                        String[] errorSoon={"email", "phone","password"};
                        JSONObject allerrors= jObj.getJSONObject("error");
                        String message = allerrors.getJSONArray("email").get(0).toString();
                        Log.e(TAG, "onResponse: tangazoletu " + message);
                            Toast.makeText(Register.this, message, Toast.LENGTH_SHORT).show();
//                        JSONObject email_error = allerrors.getj.getJSONArray("email");
//                        JSONArray email_error = allerrors.getJSONArray("email");
//                        JSONObject message1 = (JSONObject) email_error.get(0);

                        //JSONArray phone_error = allerrors.getJSONArray("phone");
//                        JSONObject messagep = (JSONObject) email_error.get(0);

                        //JSONArray pass_error = allerrors.getJSONArray("password");
//                        JSONObject messagepass = (JSONObject) email_error.get(0);
//                           if(email_error.toString()==errorSoon[0]){
//                               Toast.makeText(getApplicationContext(),message1.toString(), Toast.LENGTH_LONG).show();
//                           }
//                           else if(phone_error.toString()==errorSoon[1]){
//                               Toast.makeText(getApplicationContext(),messagep.toString(), Toast.LENGTH_LONG).show();
//                           }
//                           else if(phone_error.toString()==errorSoon[2]){
//                               Toast.makeText(getApplicationContext(),messagepass.toString(), Toast.LENGTH_LONG).show();
//                           }
                    }
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Registration Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), "could not process request" +error, Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("name", name);
                params.put("email", email);
                params.put("phone", phone);
                params.put("password", password);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}

