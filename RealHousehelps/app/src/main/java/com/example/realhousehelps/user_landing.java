package com.example.realhousehelps;
//import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.drawerlayout.widget.DrawerLayout;
import com.android.volley.RequestQueue;
import com.example.realhousehelps.helper.session_manager;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import android.widget.ArrayAdapter;
import com.google.android.material.navigation.NavigationView;
import java.util.ArrayList;
import java.util.HashMap;

public class user_landing extends AppCompatActivity {
    DrawerLayout mdrawerLayout;
    ActionBarDrawerToggle drawerToggle;
    NavigationView navigation;
   session_manager session;
   TextView user_text;
    private RequestQueue queue;
    MaterialBetterSpinner spinnerlocation;
    ArrayList<String> listItems=new ArrayList<>();
    ArrayAdapter<String> adapter;
   CardView househelps;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_landing);
        househelps=findViewById(R.id.househelps);
        househelps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),househelp_listing.class));
            }
        });
//         spinnerlocation = (MaterialBetterSpinner)
//                findViewById(R.id.filter_location);
//        ArrayAdapter<String> arrayAdapter2 = new ArrayAdapter<String>(this,
//                android.R.layout.simple_dropdown_item_1line, Type);
//        MaterialBetterSpinner mytype = (MaterialBetterSpinner)
//                findViewById(R.id.filter_type);
//        mytype.setAdapter(arrayAdapter2);
//
//        adapter=new ArrayAdapter<String>(this,R.layout.spinner_list,R.id.spinner_list_item,listItems);
//        spinnerlocation.setAdapter(adapter);
        session= new session_manager(this);
        user_text=(TextView)findViewById(R.id.user_text);
        HashMap<String, String> user = session.getUserDetails();
        String name = user.get(session_manager.KEY_NAME);
        user_text.setText("Welcome " +name);
        initInstances();
    }


    private void initInstances() {

        mdrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        drawerToggle = new ActionBarDrawerToggle(this, mdrawerLayout, R.string.open, R.string.close);
        mdrawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();
        (getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        navigation = (NavigationView) findViewById(R.id.navigation_view);
        navigation.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                int id = menuItem.getItemId();
                switch (id) {
                    case R.id.profile:
                        startActivity(new Intent(getApplicationContext(),user_profile.class));
                        // add navigation drawer item onclick method here
                        break;
                    case R.id.notifications:

                        break;
                    case R.id.payments:
                        //Do some thing here
                        // add navigation drawer item onclick method here
                        break;
                    case R.id.help:

                        break;
                    case R.id.settings:
                        //Do some thing here
                        // add navigation drawer item onclick method here
                        break;
                    case R.id.logout:
                        session.logoutUser();
                        break;
                }
                return true;
            }
        });
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (drawerToggle.onOptionsItemSelected(item))
            return true;

        return super.onOptionsItemSelected(item);
    }
}

