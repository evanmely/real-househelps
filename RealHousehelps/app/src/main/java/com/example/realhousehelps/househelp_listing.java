package com.example.realhousehelps;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.widget.*;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.realhousehelps.househelps_model;
import com.example.realhousehelps.model.Spinner_model;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
;

public class househelp_listing extends AppCompatActivity {
    String MAIN_URL = "https://0f70bcce.ngrok.io/api/househelps?page=6";
    androidx.appcompat.widget.SearchView searchView;
    private ArrayList<Spinner_model> spinnerModelArrayList;
    private ArrayList<String> names = new ArrayList<String>();

    ArrayList<househelps_model> houseList = new ArrayList<>();


    ProgressDialog progressDialog;
househelps_adapter adapter;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_househelp_listing);
            //getting the recyclerview from xml
            //recyclerView = findViewById(R.id.recyclerview);
            progressDialog = new ProgressDialog(this);
            progressDialog.setCancelable(false);

            recyclerView = findViewById(R.id.rec);
            LinearLayoutManager manager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(manager);
            recyclerView.setHasFixedSize(false);
            //initializing the houseList

            //this method will fetch and parse json
            //to display it in recyclerview
            loadHousehelps();
        }
        private void loadHousehelps () {
            progressDialog.setMessage("Loading HouseHelps ...");
            progressDialog.show();
            StringRequest stringRequest = new StringRequest(Request.Method.GET, MAIN_URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                //converting the string to json array object
                                JSONObject obj = new JSONObject(response);
                                JSONArray array = obj.getJSONArray("data");
                                //Toast.makeText(househelplisting.this, "Data Received " + array, Toast.LENGTH_LONG).show();
                                //traversing through all the object
                                for (int i = 0; i < array.length(); i++) {
                                    //getting product object from json array
                                    JSONObject househelp = array.getJSONObject(i);
                                    //adding the househelps to  view

                                    houseList.add(new househelps_model(
                                            househelp.getInt("id"),
                                            househelp.getString("first_name"),
                                            househelp.getString("marital_status"),
                                            househelp.getString("dob"),
                                            househelp.getString("gender"),
                                            househelp.getString("type_of_placement"),
                                            househelp.getString("passport_photo")

                                    ));
                                }

                               // creating adapter object and setting it to recyclerview
                                adapter = new househelps_adapter(househelp_listing.this, houseList);
                                recyclerView.setAdapter(adapter);
                                progressDialog.dismiss();
                               // adapter.setOnItemClickListener(this);
                            } catch (JSONException e) {
                                Toast.makeText(househelp_listing.this, "ERROR" + e, Toast.LENGTH_LONG).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e(" error",error.toString());
                        }
                    });

            //adding our stringrequest to queue
            Volley.newRequestQueue(this).add(stringRequest);
        }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView =(androidx.appcompat.widget.SearchView) menu.findItem(R.id.action_search)
                .getActionView();
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                adapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                adapter.getFilter().filter(query);
                return false;
            }
        });
        return true;

    }
    }

