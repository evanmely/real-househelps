package com.example.realhousehelps;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.text.Layout;
import android.view.View;
import android.widget.*;
import android.os.Bundle;
import android.view.MenuItem;

import com.example.realhousehelps.helper.session_manager;

import java.util.HashMap;

public class user_profile extends AppCompatActivity {
session_manager session;
private TextView myemail,myname,myphone,mylast_login;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        LinearLayout edit_profile = (LinearLayout )findViewById(R.id.profilePostsContainer);
        edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),Edit_profile.class));
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        myemail=(TextView) findViewById(R.id.email_tv);
        myname=(TextView)findViewById(R.id.name_tv);
        myphone=(TextView)findViewById(R.id.phone_tv);
        mylast_login=(TextView)findViewById(R.id.lastlogin_tv);
        session=new  session_manager(this);
        HashMap<String, String> user = session.getUserDetails();

        String name = user.get(session_manager.KEY_NAME);
        String email = user.get(session_manager.KEY_EMAIL);
        String phone = user.get(session_manager.KEY_PHONE);
        String last_login = user.get(session_manager.KEY_CREATED_AT);
        myemail.setText(email);
        myname.setText(name);
        myphone.setText(phone);
        mylast_login.setText(last_login);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
