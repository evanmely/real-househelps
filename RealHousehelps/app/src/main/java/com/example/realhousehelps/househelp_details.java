package com.example.realhousehelps;

import android.app.ProgressDialog;

import android.content.Intent;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.realhousehelps.APIs.APIs;

import org.json.JSONException;
import org.json.JSONObject;
import android.widget.*;

public class househelp_details extends AppCompatActivity {

    ProgressDialog progressDialog;
    Button btnrequest;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_househelp_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        btnrequest=findViewById(R.id.request);
loadHousehelps();
btnrequest.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        startActivity(new Intent(getApplicationContext(),Payment.class));
    }
});

}
        public void loadHousehelps () {
            int number =  getIntent().getIntExtra("id",0);


            //Toast.makeText(this, ""+number, Toast.LENGTH_LONG).show();
            progressDialog.setMessage("Loading HouseHelp Profile...");
            progressDialog.show();
            StringRequest stringRequest = new StringRequest(Request.Method.GET, APIs.profile_url +number,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                String url="http://192.168.0.215/thumbnail/";
                                //converting the string to json array object
                                JSONObject obj = new JSONObject(response);
                                JSONObject array = obj.getJSONObject("data");

                            TextView ftname = findViewById(R.id.fname);
                            TextView gender = findViewById(R.id.gender);
                            TextView status = findViewById(R.id.marriage);
                            TextView textdob = findViewById(R.id.dob);
                            TextView bio = findViewById(R.id.bio);
                            TextView lname = findViewById(R.id.lname);
                            ImageView image= findViewById(R.id.profile);
                            TextView placement = findViewById(R.id.placement);
                                    ftname.setText(array.getString("first_name"));
                                    lname.setText(array.getString("last_name"));
                                    gender.setText(array.getString("gender"));
                                    status.setText(array.getString("marital_status"));
                                    placement.setText(array.getString("type_of_placement"));
                                    textdob.setText(array.getString("dob"));
                                    bio.setText(array.getString("bio"));
                                    String url_image=array.getString("passport_photo");
               Glide.with(getApplicationContext())
               .load(APIs.Passport_Url+url_image)
                .thumbnail(0.5f)
                .fitCenter()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
               .into(image);

progressDialog.dismiss();
                            } catch (JSONException e) {
                                Toast.makeText(househelp_details.this, "ERROR" + e, Toast.LENGTH_LONG).show();

                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e(" error",error.toString());
                        }
                    });

            //adding our stringrequest to queue
            Volley.newRequestQueue(this).add(stringRequest);


        }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    }

